/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   controlsum.cpp
 * Author: danmax
 *
 * Created on 15 марта 2019 г., 23:38
 */

#include <iostream>
#include <cmath>
#include <complex>
#include "BinPolymon.h"

using namespace std;

BinPolymon encode(BinPolymon v, BinPolymon base){
    BinPolymon c = v.rotateleft(base.deg()) % base;
    return v.rotateleft(base.deg()) + c;
}

bool decode(BinPolymon v, BinPolymon base){
    return !(v % base == 0);
}

double modelPe(BinPolymon g, int len, double p, double eps){
    srand(time(0));
    int count = 0;
    int n = (int)(9.0 / (4.0 * eps * eps));
    for(int i = 0; i < n; i++){
        BinPolymon m(0);
        BinPolymon a = encode(m, g);
        BinPolymon e(len, p);
        a += e;
        if(e != 0 && !decode(a, g)){
            count++;
        }
    }
    return(double)count / n;
}

vector<BinPolymon> getCodes(int len, BinPolymon base){
    vector<BinPolymon> res = vector<BinPolymon>();
    BinPolymon i = BinPolymon(0);
    for(; i != BinPolymon(len, 1); ++i){
        res.push_back(encode(i, base));
    }
    res.push_back(encode(i, base));
    return res;
}

long long fact(int n){
    int res = 1;
    for(int i = 1; i <= n; i++){
        res *= i;
    }
    return res;
}

int combination(int n, int k){
    return fact(n) / (fact(k) * fact(n - k));
}

double upPe(int n, int d, double p){
    double sum = 0;
    for(int i = 0; i < d; i++){
        sum += (combination(n, i) * pow(p, i) * pow(1 - p, n - i));
    }
    return 1.0 - sum;
}

int minD(vector<BinPolymon> codes){
    int min = 10000;
    for(int i = 1; i < codes.size(); ++i){
        if(codes[i].weight() != 0 && codes[i].weight() < min){
            min = codes[i].weight();
        }
    }
    return min;
}

double Pe(int n, double p, BinPolymon base){
    vector<BinPolymon> codes = getCodes(n - base.deg(), base);
    int d = minD(codes);
    double res = 0;
    int *a = new int[n + 1];
    for(int i = 0; i < n + 1; ++i){
        a[i] = 0;
    }
    for(int i = 0; i < codes.size(); ++i){
        ++a[codes[i].weight()];
    }
    for(int i = d; i < n; ++i){
        res += a[i] * pow(p, i) * pow(1 - p, n - i);
    }
    delete [] a;
    return res;
}

int main(int argc, char** argv){
    try{
        BinPolymon g = BinPolymon("1011");
        int n = 3;
        FILE *f = fopen("data.txt", "w");
        fprintf(f, "%s\t%s\t%s\t%s\t%s\n", "p", "mod", "pe+", "pe", "abs(mod - pe)");
        for(double p = 0; p <= 1.0; p += 0.005){
            vector<BinPolymon> res = getCodes(n, g);
            int d = minD(res);
            double modp = modelPe(g, n + g.deg(), p, 0.01);
            double peup = upPe(n + g.deg(), d, p);
            double pe = Pe(n + g.deg(), p, g);
            fprintf(f, "%f\t%f\t%f\t%f\t%f\n", p, modp, peup, pe, abs(modp - pe));
            cout << "\rrun for p = " << p;
        }
    }
    catch(exception &e){
        cout << e.what() << endl;
    }
    return 0;
}

