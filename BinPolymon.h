/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinPolymon.h
 * Author: danmax
 *
 * Created on 15 марта 2019 г., 23:39
 */

#ifndef BINPOLYMON_H
#define BINPOLYMON_H

#include <vector>
#include <string>
#include <stdexcept>
#include <regex>
#include <iostream>
#include <time.h>
#include <bitset>
#include <cmath>

class BinPolymon {
private:
    std::vector<bool>arr;
    void checkempty();
    std::string toBin(int in);
    int toInt();
    BinPolymon(bool *ar, int lenght);
public:
    BinPolymon(int in);
    BinPolymon(std::string in);
    BinPolymon(int len, double p);
    BinPolymon(const BinPolymon& orig);
    virtual ~BinPolymon();
    friend std::ostream& operator<<(std::ostream& out, const BinPolymon &BinPolymon);
    int deg() const;
    BinPolymon& operator+=(const BinPolymon& right);
    BinPolymon operator+(const BinPolymon& right) const;
    BinPolymon rotateleft(int) const;
    BinPolymon& operator%=(const BinPolymon& right);
    BinPolymon operator%(const BinPolymon& right) const;
    bool operator==(const BinPolymon& right) const;
    bool operator==(const int right) const;
    bool operator==(const std::string right) const;
    bool operator!=(const BinPolymon& right) const;
    bool operator!=(const int right) const;
    bool operator!=(const std::string right) const;
    BinPolymon& operator++();
    int weight();
};
#endif /* BINPOLYMON_H */

