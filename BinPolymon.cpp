/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinPolymon.cpp
 * Author: danmax
 * 
 * Created on 15 марта 2019 г., 23:39
 */

#include "BinPolymon.h"

BinPolymon::BinPolymon(std::string in){
    arr = std::vector<bool>();
    for(int i = in.length() - 1; i >= 0; i--){
        if(in[i] == '0'){
            arr.push_back(false);
        }
        else if(in[i] == '1'){
            arr.push_back(true);
        }
        else{
            throw std::invalid_argument("invalid string");
        }
    }
    checkempty();
}

void BinPolymon::checkempty(){
    for(int i = arr.size() - 1; i > 0; i--){
        if(!arr[i]){
            arr.pop_back();
        }
        else{
            break;
        }
    }
}

BinPolymon::BinPolymon(const BinPolymon& orig){
    arr = std::vector<bool>();
    for(int i = 0; i < orig.arr.size(); i++){
        arr.push_back(orig.arr[i]);
    }
}

BinPolymon::~BinPolymon(){
}

std::ostream& operator<<(std::ostream& os, const BinPolymon& obj){
    os << "[";
    for(int i = obj.arr.size() - 1; i >= 0; i--){
        if(obj.arr[i])
            os << "1";
        else
            os << "0";
    }
    os << "]";
    return os;
}

BinPolymon::BinPolymon(int len, double p){
    arr = std::vector<bool>();
    double temp;
    for(int i = 0; i < len; i++){
        temp = (double)rand() / RAND_MAX;
        arr.push_back(temp < p);
    }
    checkempty();
}

BinPolymon::BinPolymon(bool* ar, int lenght){
    arr = std::vector<bool>();
    for(int i = 0; i < lenght; i++){
        arr.push_back(ar[i]);
    }
}

BinPolymon::BinPolymon(int in){
    arr = std::vector<bool>();
    if(in == 0){
        arr.push_back(false);
    }
    while(in > 0){
        arr.push_back(in % 10);
        in /= 10;
    }
}

int BinPolymon::deg() const{
    return arr.size() - 1;
}

BinPolymon& BinPolymon::operator+=(const BinPolymon& right){
    int lenght = (this->deg() >= right.deg()) ? this->deg() + 1 : right.deg() + 1;
    int min = (this->deg() < right.deg()) ? this->deg() + 1 : right.deg() + 1;
    int i = 0;
    for(; i < min; i++){
        this->arr[i] = this->arr[i] ^ right.arr[i];
    }
    if(min == this->deg() + 1){
        for(; i < lenght; i++){
            this->arr.push_back(right.arr[i]);
        }
    }
    this->checkempty();
    return *this;
}

BinPolymon BinPolymon::operator+(const BinPolymon& right) const{
    BinPolymon result(*this); // Создать копию себя.
    result += right; // Повторно использовать составное назначение
    return result;
}

BinPolymon BinPolymon::rotateleft(int in)const{
    BinPolymon result(*this);
    for(int i = 0; i < in; i++){
        result.arr.insert(result.arr.begin(), false);
    }
    return result;
}

BinPolymon& BinPolymon::operator%=(const BinPolymon& right){
    if(this->deg() >= right.deg()){
        BinPolymon result(*this);
        while(result.deg() >= right.deg()){
            BinPolymon t = right.rotateleft(result.deg() - right.deg());
            result += t;
        }
        result.checkempty();
        *this = result;
    }
    return *this;
}

BinPolymon BinPolymon::operator%(const BinPolymon& right) const{
    BinPolymon result(*this); // Создать копию себя.
    result %= right; // Повторно использовать составное назначение
    return result;
}

bool BinPolymon::operator==(const BinPolymon& right) const{
    if(this->deg() != right.deg()){
        return false;
    }
    else{
        for(int i = 0; i < this->arr.size(); i++){
            if(this->arr[i] != right.arr[i]){
                return false;
            }
        }
        return true;
    }
}

bool BinPolymon::operator==(const int right)const{
    return *this == BinPolymon(right);
}

bool BinPolymon::operator==(const std::string right) const{
    return *this == BinPolymon(right);
}

bool BinPolymon::operator!=(const BinPolymon& right) const{
    return !(*this == right);
}

bool BinPolymon::operator!=(const int right)const{
    return !(*this == BinPolymon(right));
}

bool BinPolymon::operator!=(const std::string right) const{
    return !(*this == BinPolymon(right));
}

std::string BinPolymon::toBin(int in){
    return std::bitset<16>(in).to_string();
}

int BinPolymon::toInt(){
    std::string s = "";
    for(int i = arr.size() - 1; i >= 0; i--){
        if(arr[i])
            s += "1";
        else
            s += "0";
    }
    return std::stoi(s, nullptr, 2);
}

BinPolymon& BinPolymon::operator++(){
    *this = BinPolymon(toBin(this->toInt() + 1));
    return *this;
}

int BinPolymon::weight(){
    int res = 0;
    for(int i = 0; i < arr.size(); ++i){
        if(arr[i]){
            ++res;
        }
    }
    return res;
}