set multiplot
set key left top
set xrange[0:1]
set yrange[0:1]
set xlabel "p"
set ylabel "pe"
plot 'data.txt' using 1:2 with lines title "Pe model",\
     'data.txt' using 1:3 with lines title "Pe+",\
     'data.txt' using 1:4 with lines title "Pe"
